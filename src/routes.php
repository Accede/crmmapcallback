<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/callback/[{password}]', function (Request $request, Response $response, array $args) {
    try {
    
        $this->exec->processall($args['password'], $request->getBody(), $this);       
        echo "ok";

    } catch (\Exception $e) {
        $this->logger->error("Error: " . $e->getMessage());
    }

});
