<?php

namespace CRMMap;

use Symfony\Component\Yaml\Yaml;

/**
 * Undocumented class
 */
class Config
{
    protected $configFile = null;
    protected $config = null;
/**
 * Loads the YAML Config file if it exists
 *
 * @return self
 */
    private function load()
    {
        if (empty($this->configFile)) {
            throw new \Exception("Config file not set");
        }
        $config = [];
        if (file_exists($this->configFile)) {
            $config = Yaml::parseFile($this->configFile);
        }
        $this->config = $config;
        return $this;
    }
/**
 *  Save the current config to file
 *
 * @return self
 */
    public function save($filename= '')
    {
        if (empty($filename)) {
            $filename = $this->configFile;
        }
        if (empty($filename)) {
            throw new \Exception("Config file not set");
        }
        $yaml = Yaml::dump($this->config);

        file_put_contents($filename, $yaml);
        return $this;
    }
/**
 * Set a value in the config, but does not save
 *
 * @param STRING $what
 * @param MULTI $to
 * @return self
 */
    public function set( $what, $to)
    {
        $this->config[$what] = $to;

       return $this;
    }
/**
 * Load and get a value from the YAML Config file
 *
 * @param STRING $what
 * @return  MULTI value
 */
    public function get( $what, $default = null)
    {
        if (isset($this->config[$what])) {
            return $this->config[$what];
        }
        return $default;
    }

/**
 * Set the name of the config file for the instance
 *
 * @param STRING $configFile
 * @return self
 */
    public function setConfigFile( $configFile)
    {
        $this->configFile = $configFile;
        if (\file_exists($configFile)) {
            $this->load();
        }

        return $this;
    }
}
