<?php

namespace CRMMap;

/**
 * Undocumented class
 */
class Exec
{

    public function processall($password, $body, $app)
    {
        if ($configs = $app->config->get('configs')) {
            foreach ($configs as $value) {
                $cpass = $password;
                if (isset($value['password'])) {
                    $cpass = $value['password'];
                }
                if ($cpass == $password) {
                    $this->processone($value, $body, $app);
                }

            }
        }
    }

    public function processone($config, $body, $app)
    {

        file_put_contents($app->get('settings')['lastcallback'], $app);

        $data = json_decode($body);

        $app->logger->info("callback - $data->subject - $data->function - {$data->data->code} ");
        $storefolder = "";
        if (isset($config['storefolder'])) {
            if ($storefolder = $config['storefolder']) {

                if (!is_dir($storefolder)) {
                    $storefolder = realpath(__DIR__ . "/../..") . "/" . $storefolder;
                }

                $storefolder = $storefolder . "/" . $data->subject . "/";
                if (!file_exists($storefolder)) {
                    mkdir($storefolder);
                }
                $storefolder = $storefolder . $data->data->code . '.json';

                switch ($data->function) {
                    case 'delete':
                        unlink($storefolder);
                        break;
                    default:
                        file_put_contents($storefolder, json_encode($data->data, JSON_PRETTY_PRINT));
                        break;
                }

            }
        }

        if (isset($config['tiggers'])) {
            if ($tiggers = $config['tiggers']) {
                foreach ($tiggers as $key => $value) {

                    if ($key == $data->subject) {

                        foreach ($value as $cmd) {
                            putenv("CRM_CODE=$data->data->code");
                            putenv("CRM_FUNCTION=$data->function");
                            putenv("CRM_SUBJECT=$data->subject");
                            putenv("CRM_FILE=$storefolder");
                            
                            $cmd = \sprintf($cmd, $data->data->code, $data->function, $data->subject, $storefolder);
                            $app->logger->info($cmd );
                            $out = $this->execInBackground($cmd);

                            $app->logger->info($out );
                        }
                    }
                }

            }
        }
    }

    public function execInBackground($cmd)
    {
        try {
            if (substr(php_uname(), 0, 7) == "Windows") {
                pclose(popen("start /B " . $cmd, "r"));
            } else {
                exec($cmd . " > /dev/null &");
            }
        } catch (\Exception $e) {
            return "Error: " . $e->getMessage();
        }
        return "ok";
    }
}
